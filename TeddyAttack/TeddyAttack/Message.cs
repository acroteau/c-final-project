﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TeddyAttack
{
    class Message
    {
        #region Fields
        private string text;
        private SpriteFont font;
        private Vector2 center, position;


        #endregion
        ///<summary>
        ///Constructor for a message object
        ///</summary>
        ///<param name="text"> The string text for the message</param>
        ///<param name="font"> The SpriteFont for the message</param>
        ///<param name="center"> The Vector2 for the center</param>

        public Message(string text, SpriteFont font, Vector2 center)
        {
            this.text = text;
            this.font = font;
            this.center = center;
            position = new Vector2();

            //calculate position from text and center
            UpdatePosition();
        }

        #region Properties
        ///<summary>
        ///Sets the text for the message
        ///</summary>

        public string Text
        {
            set
            {
                text = value;

            }
        }

        #endregion
        #region Methods
        /// <summary>
        /// Update the location of the message object
        /// </summary>
        private void UpdatePosition()
        {
            float textWidth = font.MeasureString(text).X;
            float textHeight = font.MeasureString(text).Y;
            position.X = center.X - textWidth / 2;
            position.Y = center.Y - textHeight / 2;
        }

        /// <summary>
        /// Draws a message
        /// </summary>
        /// <param name = "spriteBatch">The game's spriteBatch</param>

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position, Color.White);
        }
        #endregion
    }
}
