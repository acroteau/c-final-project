﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TeddyAttack
{
    /// <summary>
    /// A heart can give the bear +10 health if the bear picks it up
    /// </summary>
    public class Heart
    {
        #region Fields
        bool active = true;

        // drawing support
        Texture2D sprite;
        Rectangle drawRectangle;
        Vector2 velocity;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// /// <param name="contentManager">manages content for the heart for the heart</param>
        /// <param name="spriteName">string loads image for the heart</param>
        /// <param name="x"> random x location of the center of the heart</param>
        /// <param name="y"> random y location of the center of the heart</param>
        public Heart(ContentManager contentManager, string spriteName, int x, int y)
        {
            LoadContent(contentManager, spriteName, x, y);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the collision rectangle for the heart
        /// </summary>
        public Rectangle CollisionRectangle
        {
            get { return drawRectangle; }
        }

        /// <summary>
        /// is the heart active?
        /// </summary>
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }
        /// <summary>
        /// gets velocity
        /// </summary>
        public Vector2 Velocity
        {
            get { return velocity; }
            set {velocity = value; }
        }
        #endregion

        #region private method
        /// <summary>
        /// loads the content for the heart
        /// </summary>
        /// <param name="contentManager">manages content</param>
        /// <param name="spriteName">string name for heart image</param>
        /// <param name="x">random x location</param>
        /// <param name="y">random y location</param>
        private void LoadContent(ContentManager contentManager, string spriteName,
            int x, int y)
        {
            // load content and set remainder of draw rectangle
            sprite = contentManager.Load<Texture2D>(spriteName);
            drawRectangle = new Rectangle(x - sprite.Width / 2,
                y - sprite.Height / 2, sprite.Width,
                sprite.Height);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Draws the heart
        /// </summary>
        /// <param name="spriteBatch">sprite batch</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            
            spriteBatch.Draw(sprite, drawRectangle, Color.White);

        }

        #endregion
    }
}
